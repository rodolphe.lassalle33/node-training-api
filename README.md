# node-training-api


# Editing this README

Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Description

## Authors and acknowledgment
Rodolphe Lassalle : rodolphe.lassalle33@gmail.com

## License
Open source project.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
