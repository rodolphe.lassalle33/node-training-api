const express = require('express');
const morgan = require('morgan');
const favicon = require('serve-favicon');
const bodyParser = require('body-parser');

const { success, getUniqueID } = require('./helper.js');

let trainings = require('./mock-training.js');
const app = express();
const port = 3000;

app.use(favicon(__dirname + '/favicon.ico'))
  .use(morgan('dev'))
  .use(bodyParser.json());

app.get('/', (req, res) => res.send('Hello World!'));

app.get('/api/v1/trainings', (req,res) => {
  const message = 'Voici la liste des entrainements';
  res.json(success(message, trainings));
});

app.get('/api/v1/trainings/:id', (req, res) => {
  const id = req.params.id;
  const training = trainings.find(training => training.id === parseInt(id));
  const message = 'Un entrainement a bien été trouvé';
  res.json(success(message, training));
});


app.post('/api/v1/trainings', (req,res) => {
  const id = getUniqueID(trainings);
  const trainingCreated = { ...req.body, ...{id: id, created: new Date()} };
  const message = `L'entrainement ${trainingCreated.name} a été créé`;
  res.json(success(message, trainingCreated));
});

app.put('/api/v1/trainings/:id', (req, res) => {
  const id = parseInt(req.params.id);
  const trainingUpdated = {...req.body, id: id };
  trainings = trainings.map(training => {
    return training.id === id ? trainingUpdated : training;
  });
  const message = `L'entrainement ${trainingUpdated.name} a bien été modifié.`;
  res.json(success(message, trainingUpdated));
});

app.delete('/api/v1/trainings/:id', (req, res) => {
  const id = parseInt(req.params.id);
  const trainingDeleted = trainings.find(training => training.id === id);
  trainings.filter(training => training.id !== id);
  const message = `L'entrainement ${trainingDeleted.name} a bien été supprimé.`;
  res.json(success(message, trainingDeleted));
});

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port} 🚀`);
});


// Vidéo arrété à 2h28