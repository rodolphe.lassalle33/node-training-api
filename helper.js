exports.success = (message, data) => {
  return { message, data };
};

exports.getUniqueID = (trainings) => {
  const trainingID = trainings.map(training => training.id);
  const maxID = trainingID.reduce((a,b)=> Math.max(a,b));
  const uniqueID = maxID + 1;

  return uniqueID;
};