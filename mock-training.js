const pokemons = [
  {
    id: 1,
    session: 'Agrandir l’espace de jeu effectif (largeur et profondeur)',
    proceed: 'EXERCICE',
    theme: 'KEEP_PROGRESS',
    category: 'seniors',
    effectif: 14,
    time: 20,
    tasks: [{
      objectif: 'Agrandir l’espace de jeu effectif (largeur et profondeur)',
      description: 'Toucher le capitaine = 1 pt',
      consignes: 'Jeu au sol obligatoire. Si 1 pt est marqué, celui qui a donné le ballon au capitaine prend sa place et le ballon est rendu à l\'équipe adverse. Les joueurs qui se déplacent dans les couloirs sont inattaquables et jouent en 2 touches maximum.'
    }],
    pedagogies: [{
      variables: 'Faire passer le ballon dans les 2 couloirs avant de toucher le capitaine. Limiter le nombre de touches de balle.',
      methode_pedag: 'ACTIVE - Laisser jouer, observer et questionner',
      veillerA: ['Organiser les équipes.', 'Démarquage', 'Qualité de passe.', 'Etre "patient " dans le jeu pour marquer'],
      observation: ''
    }],
    equipments: [{id: 1, name: 'Ballon', quantity: 10}, {id: 2, name: 'Ballon', quantity: 10}],
    play_area: '60mx40m',
    video: null,
    image: null,
    createdAt: new Date(),
    updatedAt: '',
  },
  {
    id: 2,
    session: 'Agrandir l’espace de jeu effectif (largeur et profondeur)',
    proceed: Proceed.EXERCICE,
    theme: Themes.KEEP_PROGRESS,
    category: 'seniors',
    effectif: 11,
    time: 20,
    tasks: [{
      objectif: 'Agrandir l’espace de jeu effectif (largeur et profondeur)',
      description: 'Toucher le capitaine = 1 pt',
      consignes: 'Jeu au sol obligatoire. Si 1 pt est marqué, celui qui a donné le ballon au capitaine prend sa place et le ballon est rendu à l\'équipe adverse. Les joueurs qui se déplacent dans les couloirs sont inattaquables et jouent en 2 touches maximum.'
    }],
    pedagogies: [{
      variables: 'Faire passer le ballon dans les 2 couloirs avant de toucher le capitaine. Limiter le nombre de touches de balle.',
      methode_pedag: 'ACTIVE - Laisser jouer, observer et questionner',
      veillerA: ['Organiser les équipes.', 'Démarquage', 'Qualité de passe.', 'Etre "patient " dans le jeu pour marquer'],
      observation: ''
    }],
    equipments: [{id: 1, name:'' }, {id: 2, name: 'Ballon'}],
    play_area: '1/2 terrain (50mx40m)',
    video: null,
    image: null,
    createdAt: new Date(),
    updatedAt: '',
  },
  {
    id: 3,
    session: 'Agrandir l’espace de jeu effectif (largeur et profondeur)',
    proceed: Proceed.EXERCICE,
    theme: Themes.KEEP_PROGRESS,
    category: 'seniors',
    effectif: 11,
    time: 20,
    tasks: [{
      objectif: 'Agrandir l’espace de jeu effectif (largeur et profondeur)',
      description: 'Toucher le capitaine = 1 pt',
      consignes: 'Jeu au sol obligatoire. Si 1 pt est marqué, celui qui a donné le ballon au capitaine prend sa place et le ballon est rendu à l\'équipe adverse. Les joueurs qui se déplacent dans les couloirs sont inattaquables et jouent en 2 touches maximum.'
    }],
    pedagogies: [{
      variables: 'Faire passer le ballon dans les 2 couloirs avant de toucher le capitaine. Limiter le nombre de touches de balle.',
      methode_pedag: 'ACTIVE - Laisser jouer, observer et questionner',
      veillerA: ['Organiser les équipes.', 'Démarquage', 'Qualité de passe.', 'Etre "patient " dans le jeu pour marquer'],
      observation: ''
    }],
    equipments: [{id: 1, name:'' }, {id: 2, name: 'Ballon'}],
    play_area: '1/2 terrain (50mx40m)',
    video: null,
    image: null,
    createdAt: new Date(),
    updatedAt: '',
  },
  {
    id: 4,
    session: 'Agrandir l’espace de jeu effectif (largeur et profondeur)',
    proceed: Proceed.EXERCICE,
    theme: Themes.KEEP_PROGRESS,
    category: 'seniors',
    effectif: 11,
    time: 20,
    tasks: [{
      objectif: 'Agrandir l’espace de jeu effectif (largeur et profondeur)',
      description: 'Toucher le capitaine = 1 pt',
      consignes: 'Jeu au sol obligatoire. Si 1 pt est marqué, celui qui a donné le ballon au capitaine prend sa place et le ballon est rendu à l\'équipe adverse. Les joueurs qui se déplacent dans les couloirs sont inattaquables et jouent en 2 touches maximum.'
    }],
    pedagogies: [{
      variables: 'Faire passer le ballon dans les 2 couloirs avant de toucher le capitaine. Limiter le nombre de touches de balle.',
      methode_pedag: 'ACTIVE - Laisser jouer, observer et questionner',
      veillerA: ['Organiser les équipes.', 'Démarquage', 'Qualité de passe.', 'Etre "patient " dans le jeu pour marquer'],
      observation: ''
    }],
    equipments: [{id: 1, name:'' }, {id: 2, name: 'Ballon'}],
    play_area: '1/2 terrain (50mx40m)',
    video: null,
    image: null,
    createdAt: new Date(),
    updatedAt: '',
  },
  {
    id: 5,
    session: 'Agrandir l’espace de jeu effectif (largeur et profondeur)',
    proceed: Proceed.EXERCICE,
    theme: Themes.KEEP_PROGRESS,
    category: 'seniors',
    effectif: 11,
    time: 20,
    tasks: [{
      objectif: 'Agrandir l’espace de jeu effectif (largeur et profondeur)',
      description: 'Toucher le capitaine = 1 pt',
      consignes: 'Jeu au sol obligatoire. Si 1 pt est marqué, celui qui a donné le ballon au capitaine prend sa place et le ballon est rendu à l\'équipe adverse. Les joueurs qui se déplacent dans les couloirs sont inattaquables et jouent en 2 touches maximum.'
    }],
    pedagogies: [{
      variables: 'Faire passer le ballon dans les 2 couloirs avant de toucher le capitaine. Limiter le nombre de touches de balle.',
      methode_pedag: 'ACTIVE - Laisser jouer, observer et questionner',
      veillerA: ['Organiser les équipes.', 'Démarquage', 'Qualité de passe.', 'Etre "patient " dans le jeu pour marquer'],
      observation: ''
    }],
    equipments: [{id: 1, name:'' }, {id: 2, name: 'Ballon'}],
    play_area: '1/2 terrain (50mx40m)',
    video: null,
    image: null,
    createdAt: new Date(),
    updatedAt: '',
  },
  {
    id: 6,
    session: 'Agrandir l’espace de jeu effectif (largeur et profondeur)',
    proceed: Proceed.EXERCICE,
    theme: Themes.KEEP_PROGRESS,
    category: 'seniors',
    effectif: 11,
    time: 20,
    tasks: [{
      objectif: 'Agrandir l’espace de jeu effectif (largeur et profondeur)',
      description: 'Toucher le capitaine = 1 pt',
      consignes: 'Jeu au sol obligatoire. Si 1 pt est marqué, celui qui a donné le ballon au capitaine prend sa place et le ballon est rendu à l\'équipe adverse. Les joueurs qui se déplacent dans les couloirs sont inattaquables et jouent en 2 touches maximum.'
    }],
    pedagogies: [{
      variables: 'Faire passer le ballon dans les 2 couloirs avant de toucher le capitaine. Limiter le nombre de touches de balle.',
      methode_pedag: 'ACTIVE - Laisser jouer, observer et questionner',
      veillerA: ['Organiser les équipes.', 'Démarquage', 'Qualité de passe.', 'Etre "patient " dans le jeu pour marquer'],
      observation: ''
    }],
    equipments: [{id: 1, name:'' }, {id: 2, name: 'Ballon'}],
    play_area: '1/2 terrain (50mx40m)',
    video: null,
    image: null,
    createdAt: new Date(),
    updatedAt: '',
  },
  {
    id: 7,
    session: 'Agrandir l’espace de jeu effectif (largeur et profondeur)',
    proceed: Proceed.EXERCICE,
    theme: Themes.KEEP_PROGRESS,
    category: 'seniors',
    effectif: 11,
    time: 20,
    tasks: [{
      objectif: 'Agrandir l’espace de jeu effectif (largeur et profondeur)',
      description: 'Toucher le capitaine = 1 pt',
      consignes: 'Jeu au sol obligatoire. Si 1 pt est marqué, celui qui a donné le ballon au capitaine prend sa place et le ballon est rendu à l\'équipe adverse. Les joueurs qui se déplacent dans les couloirs sont inattaquables et jouent en 2 touches maximum.'
    }],
    pedagogies: [{
      variables: 'Faire passer le ballon dans les 2 couloirs avant de toucher le capitaine. Limiter le nombre de touches de balle.',
      methode_pedag: 'ACTIVE - Laisser jouer, observer et questionner',
      veillerA: ['Organiser les équipes.', 'Démarquage', 'Qualité de passe.', 'Etre "patient " dans le jeu pour marquer'],
      observation: ''
    }],
    equipments: [{id: 1, name:'' }, {id: 2, name: 'Ballon'}],
    play_area: '1/2 terrain (50mx40m)',
    video: null,
    image: null,
    createdAt: new Date(),
    updatedAt: '',
  },
  {
    id: 8,
    session: 'Agrandir l’espace de jeu effectif (largeur et profondeur)',
    proceed: Proceed.EXERCICE,
    theme: Themes.KEEP_PROGRESS,
    category: 'seniors',
    effectif: 11,
    time: 20,
    tasks: [{
      objectif: 'Agrandir l’espace de jeu effectif (largeur et profondeur)',
      description: 'Toucher le capitaine = 1 pt',
      consignes: 'Jeu au sol obligatoire. Si 1 pt est marqué, celui qui a donné le ballon au capitaine prend sa place et le ballon est rendu à l\'équipe adverse. Les joueurs qui se déplacent dans les couloirs sont inattaquables et jouent en 2 touches maximum.'
    }],
    pedagogies: [{
      variables: 'Faire passer le ballon dans les 2 couloirs avant de toucher le capitaine. Limiter le nombre de touches de balle.',
      methode_pedag: 'ACTIVE - Laisser jouer, observer et questionner',
      veillerA: ['Organiser les équipes.', 'Démarquage', 'Qualité de passe.', 'Etre "patient " dans le jeu pour marquer'],
      observation: ''
    }],
    equipments: [{id: 1, name:'' }, {id: 2, name: 'Ballon'}],
    play_area: '1/2 terrain (50mx40m)',
    video: null,
    image: null,
    createdAt: new Date(),
    updatedAt: '',
  },
  {
    id: 9,
    session: 'Agrandir l’espace de jeu effectif (largeur et profondeur)',
    proceed: Proceed.EXERCICE,
    theme: Themes.KEEP_PROGRESS,
    category: 'seniors',
    effectif: 11,
    time: 20,
    tasks: [{
      objectif: 'Agrandir l’espace de jeu effectif (largeur et profondeur)',
      description: 'Toucher le capitaine = 1 pt',
      consignes: 'Jeu au sol obligatoire. Si 1 pt est marqué, celui qui a donné le ballon au capitaine prend sa place et le ballon est rendu à l\'équipe adverse. Les joueurs qui se déplacent dans les couloirs sont inattaquables et jouent en 2 touches maximum.'
    }],
    pedagogies: [{
      variables: 'Faire passer le ballon dans les 2 couloirs avant de toucher le capitaine. Limiter le nombre de touches de balle.',
      methode_pedag: 'ACTIVE - Laisser jouer, observer et questionner',
      veillerA: ['Organiser les équipes.', 'Démarquage', 'Qualité de passe.', 'Etre "patient " dans le jeu pour marquer'],
      observation: ''
    }],
    equipments: [{id: 1, name:'' }, {id: 2, name: 'Ballon'}],
    play_area: '1/2 terrain (50mx40m)',
    video: null,
    image: null,
    createdAt: new Date(),
    updatedAt: '',
  },
  {
    id: 10,
    session: 'Agrandir l’espace de jeu effectif (largeur et profondeur)',
    proceed: Proceed.EXERCICE,
    theme: Themes.KEEP_PROGRESS,
    category: 'seniors',
    effectif: 11,
    time: 20,
    tasks: [{
      objectif: 'Agrandir l’espace de jeu effectif (largeur et profondeur)',
      description: 'Toucher le capitaine = 1 pt',
      consignes: 'Jeu au sol obligatoire. Si 1 pt est marqué, celui qui a donné le ballon au capitaine prend sa place et le ballon est rendu à l\'équipe adverse. Les joueurs qui se déplacent dans les couloirs sont inattaquables et jouent en 2 touches maximum.'
    }],
    pedagogies: [{
      variables: 'Faire passer le ballon dans les 2 couloirs avant de toucher le capitaine. Limiter le nombre de touches de balle.',
      methode_pedag: 'ACTIVE - Laisser jouer, observer et questionner',
      veillerA: ['Organiser les équipes.', 'Démarquage', 'Qualité de passe.', 'Etre "patient " dans le jeu pour marquer'],
      observation: ''
    }],
    equipments: [{id: 1, name:'' }, {id: 2, name: 'Ballon'}],
    play_area: '1/2 terrain (50mx40m)',
    video: null,
    image: null,
    createdAt: new Date(),
    updatedAt: '',
  },
  {
    id: 11,
    session: 'Agrandir l’espace de jeu effectif (largeur et profondeur)',
    proceed: Proceed.EXERCICE,
    theme: Themes.KEEP_PROGRESS,
    category: 'seniors',
    effectif: 11,
    time: 20,
    tasks: [{
      objectif: 'Agrandir l’espace de jeu effectif (largeur et profondeur)',
      description: 'Toucher le capitaine = 1 pt',
      consignes: 'Jeu au sol obligatoire. Si 1 pt est marqué, celui qui a donné le ballon au capitaine prend sa place et le ballon est rendu à l\'équipe adverse. Les joueurs qui se déplacent dans les couloirs sont inattaquables et jouent en 2 touches maximum.'
    }],
    pedagogies: [{
      variables: 'Faire passer le ballon dans les 2 couloirs avant de toucher le capitaine. Limiter le nombre de touches de balle.',
      methode_pedag: 'ACTIVE - Laisser jouer, observer et questionner',
      veillerA: ['Organiser les équipes.', 'Démarquage', 'Qualité de passe.', 'Etre "patient " dans le jeu pour marquer'],
      observation: ''
    }],
    equipments: [{id: 1, name:'' }, {id: 2, name: 'Ballon'}],
    play_area: '1/2 terrain (50mx40m)',
    video: null,
    image: null,
    createdAt: new Date(),
    updatedAt: '',
  },
  {
    id: 12,
    session: 'Agrandir l’espace de jeu effectif (largeur et profondeur)',
    proceed: Proceed.EXERCICE,
    theme: Themes.KEEP_PROGRESS,
    category: 'seniors',
    effectif: 11,
    time: 20,
    tasks: [{
      objectif: 'Agrandir l’espace de jeu effectif (largeur et profondeur)',
      description: 'Toucher le capitaine = 1 pt',
      consignes: 'Jeu au sol obligatoire. Si 1 pt est marqué, celui qui a donné le ballon au capitaine prend sa place et le ballon est rendu à l\'équipe adverse. Les joueurs qui se déplacent dans les couloirs sont inattaquables et jouent en 2 touches maximum.'
    }],
    pedagogies: [{
      variables: 'Faire passer le ballon dans les 2 couloirs avant de toucher le capitaine. Limiter le nombre de touches de balle.',
      methode_pedag: 'ACTIVE - Laisser jouer, observer et questionner',
      veillerA: ['Organiser les équipes.', 'Démarquage', 'Qualité de passe.', 'Etre "patient " dans le jeu pour marquer'],
      observation: ''
    }],
    equipments: [{id: 1, name:'' }, {id: 2, name: 'Ballon'}],
    play_area: '1/2 terrain (50mx40m)',
    video: null,
    image: null,
    createdAt: new Date(),
    updatedAt: '',
  }
];

module.exports = pokemons;
